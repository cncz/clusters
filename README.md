# RU meeting on Compute Clusters


> C&CZ managed clusters
>
> C&CZ: part of Faculty of Science (FNWI)
>
> C&CZ: IT for Science Research and Education

Peter van Campen & Ben Polman, C&CZ

December 6, 2018

> URL [https://wiki.cncz.science.ru.nl/clusters/](https://wiki.cncz.science.ru.nl/clusters/)
> 
> Google(cncz+cluster)

---

# Ancient history
> 
> Started early 2006: cn-cluster (cn for compute node / cluster node)
> 
> Departments bought Sun Fire X4100 dual-processor dual-core machines
>
> C&CZ installed Fedora Linux with Sun GridEngine
>
> FNWI pays for **everything** but the hardware: housing/hosting/installation/management
>

### Please feel free to interrupt with questions/remarks!
 
---

# Growth and different clusters/owners
- 2006: cn-cluster, heterogeneous (1 has 3 TB RAM), owned by 12 research groups:
    - Now: 65 nodes, 1632 cores, 9 TB RAM. Some GPU's with CUDA cores 
- 2008: CNS/neuro: turnkey ClusterVision cluster
    - 40 Gb/s Infiniband and 2 mainboards in 1U
    - patron/peons now owned by NeuroInformatics
    - requirements for non-FNWI services
- 2012: Astrophysics ERC grant: cluster with Infiniband and GPU's
    - Now: 920 cores, 7.3 TB RAM, 600 TB on fileservers
- 2012: CLS (Faculty of Arts)
    - Now: 320 cores, 2.2 TB RAM 
- 2012: DCC (Social Sciences)
    - Now: 208 cores, 2.4 TB RAM
---


# Hardware brands and software choices
> 
> Sun -> Supermicro -> Dell (primarily)
> 
> Solaris -> Fedora -> Ubuntu
> 
> GridEngine -> Torque/MAUI -> Slurm


---


# Why chosen?
> 
> Dell:
    - better designed hardware and support
    - old servers support: Econocom (3x cheaper)
> Ubuntu:
    - LTS gets support and security patches for 5 years
    - one OS for servers and desktops
> Slurm:
    - support for cgroups (jobs stay constrained to initial allocation)
    - able to suspend jobs (if enough memory )
    - partitions (queues):
            - settings for default and maximum: memory, time, cpu
            - department / priority
            - suspendable or not / requeuing
            - all.q: very nice for low-prio short-running jobs
            - ...
---


# Questions?
> 
> - Sharing Intel Compiler licenses?
> - ...
> - ...
